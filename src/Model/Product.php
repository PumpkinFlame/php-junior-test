<?php

namespace WebApp\Model;

class Product
{
    private $sku;
    private $name;
    private $price;
    private $value;
    private $attributeValue;
    private $attributeName;

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue(string $value)
    {
        $this->value = $value;
    }

    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    public function setAttributeValue(string $attributeValue)
    {
        $this->attributeValue = $attributeValue;
    }

    public function getAttributeName()
    {
        return $this->attributeName;
    }

    public function setAttributeName(string $attributeName)
    {
        $this->attributeName = $attributeName;
    }

    public function getValueDetail()
    {
        return $this->attributeName. ' '. $this->value. ' '. $this->attributeValue;
    }
}
