<?php

namespace WebApp\Model;

class Database
{
    private $host = 'localhost';
    private $name = 'scandiweb';
    private $username = 'root';
    private $password = '';

    private $pdo = null;
    private $stmt = null;
    
    function __construct()
    {
        if ($this->connect() === false)
        {
            $this->createDatabase($this->name);
            $this->connect();
        }
    }
    
    function __destruct()
    {
        $this->stmt = null;
        $this->pdo = null;
    }

    function connect()
    {
        try
        {
            $this->pdo = new \PDO(
                "mysql:host=$this->host;dbname=$this->name;",
                $this->username,
                $this->password);

            return true;
        }
        catch (\PDOException $ex)
        {
            return false;
        }
    }

    function createDatabase($name)
    {
        $this->pdo = new \PDO("mysql:host=$this->host;",$this->username);
        $this->pdo->query("CREATE DATABASE $name;");
    }
 
    function query($sql)
    {
        try 
        {
            $this->stmt = $this->pdo->prepare($sql);
            $this->stmt->execute();
        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }
    }

    function queryFetchAll($sql): array
    {
        try 
        {
            $this->stmt = $this->pdo->prepare($sql);
            $this->stmt->execute();
            $arr = $this->stmt->fetchAll();
        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }

        $this->stmt = null;
        return $arr;
    }
}
