<?php

if (isset($_POST['product-type'])) 
{
    $type = $_POST['product-type'];

    if (isset($_POST['sku']) and isset($_POST['name']) and isset($_POST['price']))
    {
        $value = postValue($type);

        if ($value != null)
        {
            $productRepository = new \WebApp\Model\ProductRepository();

            $productRepository->addProduct($_POST['sku'], $_POST['name'], $_POST['price'], $type, $value);
        }
    }
}

function postValue($type)
{
    switch ($type)
    {
        case 1:
        {
            if (isset($_POST['size']))
            {
                return $_POST['size'];
            }
        }       
        break;
        case 2:
        {
            if (isset($_POST['weight']))
            {
                return $_POST['weight'];
            }
        }       
        break;
        case 3:
        {
            if (isset($_POST['height']) and isset($_POST['width']) and isset($_POST['length']))
            {
                return $_POST['height'].'x'.$_POST['width'].'x'.$_POST['length'];
            }
        }       
        break;
        default: return null;
    }
}
