<?php

namespace WebApp\Controller;

use WebApp\Model\ProductRepository;

class Product
{
    public function displayProductListing()
    {
        include_once __DIR__.'/Product/Del.php';

        $productRepository = new ProductRepository();
        $products = $productRepository->fetchProducts();

        require_once __DIR__ . '/../View/product_listing.phtml';
    }

    public function displayProductAddForm()
    {
        include_once __DIR__.'/Product/Add.php';
        
        require_once __DIR__ . '/../View/product_add_form.phtml';
    }

    public function displayPageNotFound()
    {
        require_once __DIR__ . '/../View/page_not_found.phtml';
    }
}
