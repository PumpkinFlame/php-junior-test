<?php

require __DIR__ . '/src/autoload.php';

$uri = basename($_SERVER['REQUEST_URI']);

$productController = new \WebApp\Controller\Product();
$setup = new \WebApp\Setup\InstallSchema();

switch($uri)
{
    case 'scandi':
    {
        $productController->displayProductListing();
    }
    break;
    case 'index.php?product_listing':
    {
        $productController->displayProductListing();
    }
    break;
    case 'index.php?product_add_form':
    {
        $productController->displayProductAddForm();
    }
    break;
    default:
    {
        $productController->displayPageNotFound();
    } 
    break;
}
